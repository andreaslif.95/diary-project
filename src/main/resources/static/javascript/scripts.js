//Changes the image src to the newly picked one.
function changeImageSrc() {
    document.getElementById("imagetagi").src = document.getElementById("exampleInputEmail1").value
    return false;
}
//Used so that we can access the imageHTML everwhere.
let imageHTML = ""

/**
 * Function to save the newly edited/created diary.
 * Will check so that variables aare in a accepted form.
 * Will use a tooltip to print errors otherwise.
 * Lastly it uses fetch to sned a post/patch request the the server
 * The server then responds and the user will be redirected to a redirecter which is used to embed a message to the actual destination page.
 * @param id - Empty or the id of the diary to be updated.
 */
function save(id = "") {
    const date = document.getElementById("date").innerText
    let url = document.getElementById("imagetagi").src
    let text = document.getElementById("textHolder123").innerText
    const title = document.getElementById("title").innerText
    let signedBy = document.getElementById("signedBy").innerText

    let errorMessge = ""
    if(text.toLowerCase() === "enter text here..." || text.length < 1) {
        errorMessge += "\n\tThe text or is empty."
    }
    if(title.trim().length === 0) {
        errorMessge += "\n\tTitle is empty."
    }
    if(title === "Enter title here") {
        errorMessge += "\n\tTitle cant have default value."
    }
    if(signedBy.trim().length === 0 || signedBy === "Enter author here") {
        signedBy = "Unkownn"
    }

    var dateOk=(document.getElementById("date").innerText).match(/^\d{4}-\d{2}-\d{2}$/);
    if(!dateOk) {
        errorMessge += "\n\tInvalid date, should be in the format (YYYY-MM-DD)"
    }
    if(errorMessge !== "")
    {
        alert(errorMessge)
        return
    }
    if(url.includes("/resources/404.png")) {
        url = ""
    }
    const diaryEntry = {
        title: title,
        text: text,
        url: url,
        date: date,
        signedBy: signedBy
    }
    let METHOD = (id === "" ? "POST" : "PATCH")
    if(METHOD === 'PATCH') {
        diaryEntry.id = id
    }
    let error = false;
    fetch("/diaryEntry/" + id, {
        method: METHOD,
        body: JSON.stringify(diaryEntry),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
    })
        .then(resp => {
            if(resp.status !== (METHOD === "PATCH" ? 200 : 201)) {
                error = true;
            }

            return resp.json()
        })
        .then(json =>
        {
            if(error) {
                alert("Failed to save (" + json.message + ")")
            }
            else {
                window.location.href = "/redirectView/" + json.data.id + "/" + (METHOD === "PATCH" ? "edit" : "create")
            }
        })
        .catch(err => {
            alert("Failed to save due to server errors.")
        })

}

/**
 * Will enable a popover over the button with the class "saveButton".
 * @param message - Message to be shown withtin the popover.
 */
function alert(message) {
    $('#saveButton').attr('data-content', message).popover('show')
    setTimeout(() => {
        $('#saveButton').attr('data-content', message).popover('hide')
    }, 3500)
}

/**
 * Used to append text into our text holder.
 * This function is used instead of inserting it instantly with thymeleaf since we need to actually change the text inside before we apply it.
 * @param text - Text to be shown.
 * @param url - Url of the pic.
 */
function appendText(text, url) {
    console.log(url)
    if(!validURL(url)) {
        console.log('Not valid')
        url = "";
    }
    document.getElementById("textHolder123").innerText = text.replace('\\n', "<br>");
    imageHTML = "<span id=\"imageSpan\" contenteditable=false style=\"float:right\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Click on the image to change url\"><img onerror=\"this.src='../resources/404.png';\" data-toggle=\"modal\" data-target=\"#staticBackdrop\" id=\"imagetagi\" src=\""+url+"\" style=\"max-width: 30vw; max-height: 30vh; display:block; margin: 10px auto; float:right;\"></span>"
    document.getElementById("textHolder123").innerHTML = imageHTML + document.getElementById("textHolder123").innerHTML
    $('[data-toggle="tooltip"]').tooltip({ trigger:'hover' });
}

// Controlls if the string is a valid url.
function validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
        '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
}

/**
 * Controlls input area for the textHolder.
 *
 * Adds the img tag to the top of the text each time if the text is empty.
 * Triggers a tooltip aswell.
 */
function checkInputs() {
    if(imageHTML === "") {
        imageHTML = "<span id=\"imageSpan\" contenteditable=false style=\"float:right\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Click on the image to change url\"><img onerror=\"this.src='../resources/404.png';\" data-toggle=\"modal\" data-target=\"#staticBackdrop\" id=\"imagetagi\" src=\"\" style=\"max-width: 30vw; max-height: 30vh; display:block; margin: 10px auto; float:right;\"></span>"
    }

    else {
        if (document.getElementById("textHolder123").innerText.trim() === "") {
            document.getElementById("textHolder123").innerText = "enter text here..."
            document.getElementById("textHolder123").innerHTML = imageHTML + document.getElementById("textHolder123").innerHTML
            $('[data-toggle="tooltip"]').tooltip({trigger: 'hover'});
        }
    }

}

/**
 * Enters default text to title author and date if they're empty.
 * @param e
 */
function notEmpty(e) {
    if(e.innerText.trim() === "") {
        if(e.id.includes("date")) {
            setDate()
        }
        else {
            let string = ""
            switch(e.id) {
                case 'title':
                    string = "Enter title here"
                    break;
                case 'signedBy':
                    string = "Enter author here"
            }
            e.innerText = string
        }
    }
}

/**
 * Fetches the curent date in the format (YYYY-DD-MM)
 */
function setDate() {
    let d = new Date()
    let month = d.getMonth() > 9 ? d.getMonth() : "0"+d.getMonth()
    let day = d.getDay()> 9 ? d.getDay() : "0"+d.getDay()

    document.getElementById("date").innerText = d.getFullYear() + "-" + month + "-" + day
}

/**
 * Uses the delete method to delete the specified diary entry.
 * @param id - id of the entry to be deleted.
 */
function deleteThis(id) {
    let success = true;
    if(confirm("Are you sure you would like to delete this entry?")) {
        fetch("/diaryEntry/" + id, {
            method: 'DELETE'
        }).then((resp) => {
            if (resp.status !== 200) {
                success = false
            }
            window.location.href = "/redirectIndex/" + success
        })
        .catch(err => {
            window.location.href = "/redirectIndex/" + false
        })
    }



}

/**
 * Loads all the diaries from the api. We don't use thymeleaf since we want easier controll.
 */
function loadAll(newest) {
    let error = false
    fetch("/diaryEntries/" + (newest ? "" : "old"))
        .then(resp => {
            if(resp.status !== 200) {
                error =true
            }
            return resp.json()
        })
        .then(json => {
            if(json.data.length > 0 && !error) {
                document.getElementById("tbodyHolder").innerHTML = ""
                json.data.forEach(diary => {
                    document.getElementById("tbodyHolder").innerHTML += htmlTemplate(diary)
                })
            }
            else {
                document.getElementById("tbodyHolder").innerText = "No diary entries present.."
            }

        })
}

/**
 * If we focus something with default text we set the text to be epty.
 * @param t - the html element which is focuesd.
 */
function controlFocus(t) {
    if(t.innerText === "Can't be empty" || t.innerText.toLowerCase() === "enter text here..." || t.innerText === "Enter title here" || t.innerText === "Enter author here") {
        if(t.id === "textHolder123") {
            if(imageHTML === "") {
                imageHTML = "<span id=\"imageSpan\" contenteditable=false style=\"float:right\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Click on the image to change url\"><img onerror=\"this.src='../resources/404.png';\" data-toggle=\"modal\" data-target=\"#staticBackdrop\" id=\"imagetagi\" src=\"\" style=\"max-width: 30vw; max-height: 30vh; display:block; margin: 10px auto; float:right;\"></span>"
            }
            t.innerHTML = imageHTML + t.innerText
        }
        else {
            t.innerText = ""
        }
    }
}

function htmlTemplate(diary) {
    return "<tr style=\"border-bottom: aliceblue solid\"> <td> <blockquote class=\"blockquote\"><p class=\"mb-0\" style=\"font-family: 'Antic Slab', serif;\"> " + diary.title +"</p><footer class=\"blockquote-footer\" style=\"margin-left: 5px;\">"+diary.signedBy +" " + diary.date.toString().split("T")[0] + "</footer> </blockquote> </td> <td style=\"text-align: right; margin-right: 25px;\"> <a href=\"/view/" + diary.id + "\" class=\"mb-0 menuItem\">[Open]</a> <br><a href=\"/edit/" + diary.id + "\" class=\"mb-0 menuItem\">[Edit]</a><br><a onClick='deleteThis("+ diary.id +")' class=\"mb-0 menuItem\">[Delete]</a></td> </tr>"
}