package se.experis.com.diary.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import se.experis.com.diary.models.CommonResponse;
import se.experis.com.diary.models.DiaryEntry;
import se.experis.com.diary.repositories.DiaryEntryRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Optional;

@Controller
public class ThymeLeafController {

    @Autowired
    private DiaryEntryRepository diaryEntryRepository;

    // Returns an edit page filled in with data from selected diary entry model
    // fetched using the provided id @PathVariable
    @RequestMapping(value = "/edit/{id}")
    public String editDiary(@PathVariable Integer id, Model model) {
        Optional<DiaryEntry> diaryEntityRepo = diaryEntryRepository.findById(id);
        DiaryEntry diaryEntry = diaryEntityRepo.get();
        model.addAttribute("diaryEntry", diaryEntry);
        return "edit";
    }

    // Returns a view page filled in with data from selected diary entry model
    // fetched using the provided id @PathVariable
    // Additionally, get a @ModelAttribute to check if the request was made from
    // within the server by redirect or if it was issued by the user, then add
    // a model attribute to the thymepage
    // If a redirect was made, the thymepage will display a message telling the user
    // it was successfully redirected and the reason why
    @RequestMapping(value = "/view/{id}")
    public String viewDiary(@PathVariable Integer id, Model model, @ModelAttribute("redirectedFrom") Object redirectedFrom) {
        Optional<DiaryEntry> diaryEntityRepo = diaryEntryRepository.findById(id);
        DiaryEntry diaryEntry = diaryEntityRepo.get();
        model.addAttribute("diaryEntry", diaryEntry);

        if(redirectedFrom instanceof String) {
            model.addAttribute("redirectedFrom", redirectedFrom);
        } else {
            model.addAttribute("redirectedFrom", "");
        }


        return "view";
    }

    // Returns the index page
    // Additionally, get a @ModelAttribute to check if the request was made from
    // within the server by redirect or if it was issued by the user, then add
    // a model attribute to the thymepage
    // If a redirect was made, the thymepage will display a message telling the user
    // it was successfully redirected and the reason why
    @RequestMapping(value = "/")
    public String allDiaries(Model model, @ModelAttribute("redirectedFrom") Object redirectedFrom) {
        if(redirectedFrom instanceof String) {
            model.addAttribute("redirectedFrom", redirectedFrom);
        } else {
            model.addAttribute("redirectedFrom", "");
        }
        return "index";
    }

    // Returns the new page
    @RequestMapping(value = "/new")
    public String newDiary() {
        return "new";
    }

    // Redirects the page to the view page endpoint with a redirection reason
    @GetMapping("/redirectView/{id}/{method}")
    public RedirectView redirectWithUsingRedirectView(
            @PathVariable Integer id,
            @PathVariable String method,
            RedirectAttributes attributes) {
        attributes.addFlashAttribute("redirectedFrom", method);
        return new RedirectView("/view/" + id);
    }

    // Redirects the page to the index page endpoint with a redirection reason
    @GetMapping("/redirectIndex/{success}")
    public RedirectView redirectWithUsingRedirectIndex(
            @PathVariable boolean success,
            RedirectAttributes attributes) {
        attributes.addFlashAttribute("redirectedFrom", success ? "deleted" : "failed");
        return new RedirectView("/");
    }
}
