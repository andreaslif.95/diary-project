package se.experis.com.diary.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;
import se.experis.com.diary.Utils.Command;
import se.experis.com.diary.Utils.Logger;
import se.experis.com.diary.models.CommonResponse;
import se.experis.com.diary.models.DiaryEntry;
import se.experis.com.diary.repositories.DiaryEntryRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Optional;

@RestController
public class DiaryEntityController {


    // Get autowired diary entry repository
    @Autowired
    private DiaryEntryRepository diaryEntryRepository;

    // When the JSON body isn't formatted in a way that can be bound to the model,
    // it will throw a "HttpMessageNotReadableException".
    // This most commonly occurs because the "date" string is not formatted in a way
    // that can be casted into a Date object.
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<CommonResponse> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex, HttpServletRequest request) {
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;
        cr.data = null;
        cr.message = "The request could not be read, please check if date is correctly formatted";// + ex.getMessage();
        resp = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(cr, resp);
    }

    // Gets all diary entries and returns it in a common response
    @RequestMapping(value = {"/diaryEntries/{sort}", "/diaryEntries/"}, method = RequestMethod.GET)
    public ResponseEntity<CommonResponse> getAllDiaryEntries(HttpServletRequest request, @PathVariable(required = false) String sort) {
        Command cmd = new Command(request);

        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        // Call "findAll" in the repository to return all entries
        ArrayList<DiaryEntry> allEntries = (ArrayList<DiaryEntry>) diaryEntryRepository.findAll();
        if(sort != null && sort.equals("old")) {
            // Sort from oldest to newest
            allEntries.sort((d1,d2) -> d1.date.compareTo(d2.date));
        } else {
            // Sort from newest to oldest
            allEntries.sort((d1,d2) -> d2.date.compareTo(d1.date));
        }
        cr.data = allEntries;
        cr.message = "All diary entries";
        resp = HttpStatus.OK;

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    // Get a specific diary entry using an id
    // Will throw a "NumberFormatException" if the @PathVariable is not an Integer
    @GetMapping("/diaryEntry/{id}")
    public ResponseEntity<CommonResponse> getDiaryEntryById(HttpServletRequest request, @PathVariable Integer id) {
        Command cmd = new Command(request);

        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        // Check if the id exists
        if (diaryEntryRepository.existsById(id)) {
            // Fill in the entry
            cr.data = diaryEntryRepository.findById(id);
            cr.message = "Diary entry with id: " + id;
            resp = HttpStatus.OK;
        } else {
            // ... else return NOT_FOUND
            cr.data = null;
            cr.message = "No entry with id " + id + " was found.";
            resp = HttpStatus.NOT_FOUND;
        }

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);

        return new ResponseEntity<>(cr, resp);
    }

    // Adds a diary entry to the database
    // Gets all information through JSON in the request body.
    // In case the JSON information can not be casted to a "DiaryEntry", it throws a
    // "HttpMessageNotReadableException" (handled above)
    @PostMapping("/diaryEntry")
    public ResponseEntity<CommonResponse> addDiaryEntry(HttpServletRequest request, @RequestBody DiaryEntry diaryEntry) {
        Command cmd = new Command(request);

        // Save entry to repository
        diaryEntry = diaryEntryRepository.save(diaryEntry);

        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        // Return JSON of the entry as it has been added to the repository
        cr.data = diaryEntry;
        cr.message = "Added entry with id " + diaryEntry.id;
        resp = HttpStatus.CREATED;

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);

        return new ResponseEntity<>(cr, resp);
    }

    // Updates an entry using a path variable id and JSON in request body
    // Supports partly updates to the entry (i.e. only updating title
    @PatchMapping("/diaryEntry/{id}")
    public ResponseEntity<CommonResponse> updateDiaryEntry(HttpServletRequest request, @RequestBody DiaryEntry newDiaryEntry, @PathVariable Integer id) {
        Command cmd = new Command(request);

        CommonResponse cr = new CommonResponse();
        HttpStatus resp;
        // Check if the target entry exists
        if (diaryEntryRepository.existsById(id)) {
            // Get the diaryEntry
            Optional<DiaryEntry> diaryEntityRepo = diaryEntryRepository.findById(id);
            DiaryEntry diaryEntry = diaryEntityRepo.get();

            // Checks if these fields exists in the JSON body
            // and updates if it does
            if (newDiaryEntry.title != null) {
                diaryEntry.title = newDiaryEntry.title;
            }
            if (newDiaryEntry.text != null) {
                diaryEntry.text = newDiaryEntry.text;
            }
            if (newDiaryEntry.url != null) {
                diaryEntry.url = newDiaryEntry.url;
            }
            if (newDiaryEntry.date != null) {
                diaryEntry.date = newDiaryEntry.date;
            }
            if (newDiaryEntry.signedBy != null) {
                diaryEntry.signedBy = newDiaryEntry.signedBy;
            }

            // Saves to repository
            diaryEntryRepository.save(diaryEntry);

            // Return successful message
            cr.data = diaryEntry;
            cr.message = "Updated entry with id " + diaryEntry.id;
            resp = HttpStatus.OK;
        } else {
            // ... else return NOT_FOUND
            cr.data = null;
            cr.message = "No diary entry id " + id;
            resp = HttpStatus.NOT_FOUND;
        }

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);

        return new ResponseEntity<>(cr, resp);
    }

    // Deletes a target entry using a PathVariable id
    @DeleteMapping("/diaryEntry/{id}")
    public ResponseEntity<CommonResponse> deleteDiaryEntry(HttpServletRequest request, @PathVariable Integer id) {
        Command cmd = new Command(request);

        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        // Check if the entry exists
        if (diaryEntryRepository.existsById(id)) {
            // Deletes if it does exist
            diaryEntryRepository.deleteById(id);
            cr.message = "Deleted entry with id: " + id;
            resp = HttpStatus.OK;
        } else {
            // Sends error message if NOT_FOUND
            cr.message = "Diary entry with id " + id + " was not found.";
            resp = HttpStatus.NOT_FOUND;
        }

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);

        return new ResponseEntity<>(cr, resp);
    }



    // Logs requests sent to the server
    // Borrowed straight from https://gitlab.com/noroff-accelerate/java/projects/citadel/
    @GetMapping("/log")
    public ResponseEntity<ArrayList<Command>> log(HttpServletRequest request){
        Command cmd = new Command(request);

        //no common response

        //log and return
        cmd.setResult(HttpStatus.OK);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<ArrayList<Command>>(Logger.getInstance().getLog(), HttpStatus.OK);
    }

}
