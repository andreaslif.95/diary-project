package se.experis.com.diary.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.com.diary.models.DiaryEntry;

public interface DiaryEntryRepository extends JpaRepository<DiaryEntry, Integer> {
    DiaryEntry getById(String id);
}